package br.com.tsinova.redfishts;

import org.json.JSONException;
import org.json.JSONObject;

public class SystemMemorySummary {
    
    private Integer totalSystemMemoryGib;
    private String statusState;
    private String statusHealth;
    private String statusHealthrollup;
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();
        json.put("total_system_memory_gib", totalSystemMemoryGib == null ? 0 : totalSystemMemoryGib);
        json.put("status_state", statusState);
        json.put("status_health", statusHealth);
        json.put("status_healthrollup", statusHealthrollup);                
        return json;
        
    }

    public SystemMemorySummary(JSONObject jsonStart) throws Exception {                
        totalSystemMemoryGib = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, jsonStart, "MemorySummary.TotalSystemMemoryGiB", null), null);
        statusState = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "MemorySummary.Status.State", ""), "");
        statusHealth = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "MemorySummary.Status.Health", ""), "");
        statusHealthrollup = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "MemorySummary.Status.HealthRollUp", ""), "");
                
    }

    public Integer getTotalSystemMemoryGib() {
        return totalSystemMemoryGib;
    }

    public void setTotalSystemMemoryGib(Integer totalSystemMemoryGib) {
        this.totalSystemMemoryGib = totalSystemMemoryGib;
    }

    public String getStatusState() {
        return statusState;
    }

    public void setStatusState(String statusState) {
        this.statusState = statusState;
    }

    public String getStatusHealth() {
        return statusHealth;
    }

    public void setStatusHealth(String statusHealth) {
        this.statusHealth = statusHealth;
    }

    public String getStatusHealthrollup() {
        return statusHealthrollup;
    }

    public void setStatusHealthrollup(String statusHealthrollup) {
        this.statusHealthrollup = statusHealthrollup;
    }

    @Override
    public String toString() {
        return "SystemMemorySummary{" + "totalSystemMemoryGib=" + totalSystemMemoryGib + ", statusState=" + statusState + ", statusHealth=" + statusHealth + ", statusHealthrollup=" + statusHealthrollup + '}';
    }  
    
}
