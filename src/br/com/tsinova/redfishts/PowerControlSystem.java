package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import org.json.JSONException;
import org.json.JSONObject;

public class PowerControlSystem {

    /*        
    Esta é a definição do esquema para as métricas de energia. 
    Representa as propriedades para Consumo de Energia e Limitação de Potência.  
    
    path = /redfish/v1/Chassis/{id do sistema}/Power/
    
     */
    public static final String TYPE = "PowerControl";
    public static final String PREFIX = "sys_";

    private Integer powerCapacityWatts; // PowerCapacityWatts
    private Integer powerConsumedWatts; // PowerConsumedWatts
    private Integer averageConsumedWatts; // PowerMetrics.AverageConsumedWatts
    private Integer intervalInMin; // PowerMetrics.IntervalInMin
    private Integer maxConsumedWatts; // PowerMetrics.MaxConsumedWatts
    private Integer minConsumedWatts; // PowerMetrics.MinConsumedWatts    
    private String systemUuid;
    private String serverProductSerial;
    private String urlPower;

    public JSONObject toJSONObjectSummary() throws JSONException {

        JSONObject json = new JSONObject();
        json.put("power_capacity_watts", powerCapacityWatts == null ? 0 : powerCapacityWatts);
        json.put("power_consumed_watts", powerConsumedWatts == null ? 0 : powerConsumedWatts);
        json.put("average_consumed_watts", averageConsumedWatts == null ? 0 : averageConsumedWatts);
        json.put("interval_in_min", intervalInMin == null ? 0 : intervalInMin);
        json.put("max_consumed_watts", maxConsumedWatts == null ? 0 : maxConsumedWatts);
        json.put("min_consumed_watts", minConsumedWatts == null ? 0 : minConsumedWatts);
        return json;

    }

    public JSONObject toJSONObject() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(SystemRedfish.PREFIX + "uuid", systemUuid);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "power_capacity_watts", powerCapacityWatts == null ? 0 : powerCapacityWatts);
        json.put(PREFIX + "power_consumed_watts", powerConsumedWatts == null ? 0 : powerConsumedWatts);
        json.put(PREFIX + "average_consumed_watts", averageConsumedWatts == null ? 0 : averageConsumedWatts);
        json.put(PREFIX + "interval_in_min", intervalInMin == null ? 0 : intervalInMin);
        json.put(PREFIX + "max_consumed_watts", maxConsumedWatts == null ? 0 : maxConsumedWatts);
        json.put(PREFIX + "min_consumed_watts", minConsumedWatts == null ? 0 : minConsumedWatts);
        json.put("type", TYPE);   
        return json;

    }

    public PowerControlSystem(Server server, String urlSystem, String systemUuid, String serverProductSerial) throws Exception {

        this.systemUuid = systemUuid;
        this.serverProductSerial = serverProductSerial;
        this.urlPower = Util.getUrl(server.getUrl(), server.getPath(), "Chassis", Util.getPathUrl(urlSystem), "Power");

        HttpResponse<JsonNode> request = Unirest.get(urlPower).
                basicAuth(server.getUser(), server.getPassword()).
                asJson();

        if (request.getBody() == null) {
            throw new Exception("No response to " + urlPower);
        }

        JSONObject json = request.getBody().getObject();

        powerCapacityWatts = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "PowerCapacityWatts", null), null);
        powerConsumedWatts = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "PowerConsumedWatts", null), null);
        averageConsumedWatts = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "PowerMetrics.AverageConsumedWatts", null), null);
        intervalInMin = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "PowerMetrics.IntervalInMin", null), null);
        maxConsumedWatts = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "PowerMetrics.MaxConsumedWatts", null), null);
        minConsumedWatts = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "PowerMetrics.MinConsumedWatts", null), null);

    }

    public Integer getPowerCapacityWatts() {
        return powerCapacityWatts;
    }

    public void setPowerCapacityWatts(Integer powerCapacityWatts) {
        this.powerCapacityWatts = powerCapacityWatts;
    }

    public Integer getPowerConsumedWatts() {
        return powerConsumedWatts;
    }

    public void setPowerConsumedWatts(Integer powerConsumedWatts) {
        this.powerConsumedWatts = powerConsumedWatts;
    }

    public Integer getAverageConsumedWatts() {
        return averageConsumedWatts;
    }

    public void setAverageConsumedWatts(Integer averageConsumedWatts) {
        this.averageConsumedWatts = averageConsumedWatts;
    }

    public Integer getIntervalInMin() {
        return intervalInMin;
    }

    public void setIntervalInMin(Integer intervalInMin) {
        this.intervalInMin = intervalInMin;
    }

    public Integer getMaxConsumedWatts() {
        return maxConsumedWatts;
    }

    public void setMaxConsumedWatts(Integer maxConsumedWatts) {
        this.maxConsumedWatts = maxConsumedWatts;
    }

    public Integer getMinConsumedWatts() {
        return minConsumedWatts;
    }

    public void setMinConsumedWatts(Integer minConsumedWatts) {
        this.minConsumedWatts = minConsumedWatts;
    }

    public String getUrlPower() {
        return urlPower;
    }

    public void setUrlPower(String urlPower) {
        this.urlPower = urlPower;
    }

    public String getSystemUuid() {
        return systemUuid;
    }

    public void setSystemUuid(String systemUuid) {
        this.systemUuid = systemUuid;
    }

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }
    
}
