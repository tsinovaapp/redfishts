package br.com.tsinova.redfishts;

import java.time.Instant;
import org.json.JSONArray;
import org.json.JSONObject;

public class Util {
    
    public static final String NAME_DOC_ID = "id_doc";    
    
    
    public static String getDatetimeNowForElasticsearch(){
        return Instant.now().toString();
    }
    
    public static String getUrl(String url, String ... paths){
        
        String newUrl = url;
        
        // contem barra no inicio da url
        if (!containsBarEnd(url)){
            newUrl += "/";
        }
        
        for (String path : paths) {
            
            path = path.replaceAll("//", "/");
            
            // contem barra no inicio do path/caminho?
            if (containsBarStart(path)){
                
                // contem barra no final da url?
                if (containsBarEnd(newUrl)){
                    newUrl += path.substring(1);
                }else{
                    newUrl += path;
                }
                
            }else{
                
                // contem barra no final da url?
                if (containsBarEnd(newUrl)){
                    newUrl += path;
                }else{
                    newUrl += "/" + path;
                }
                
            }
            
        }
        
        if (!containsBarEnd(newUrl)){
            newUrl += "/";
        }
        
        return newUrl;
        
    }
    
    public static boolean containsBarEnd(String path){
        if (path == null || path.trim().isEmpty()){
            return false;
        }        
        return (path.charAt(path.length()-1)+"").equalsIgnoreCase("/");        
    }
    
    public static boolean containsBarStart(String path){
        if (path == null || path.trim().isEmpty()){
            return false;
        }        
        return (path.charAt(0)+"").equalsIgnoreCase("/");        
    }
    
    private static JSONObject getJson(JSONObject jsonStart, String key) throws Exception{
        
        String keys[] = key.split("\\.");
        
        if (keys.length < 2){
            return jsonStart;
        }
        
        int i = 0;
        JSONObject json = null;
        
        while (i < keys.length-1){
            if (json == null){
                json = jsonStart.getJSONObject(keys[i]);
            }else{
                json = json.getJSONObject(keys[i]);
            }
            i++;
        }
        
        return json;
        
    }
    
    public static JSONArray getJSONArrayEmpty(){
        return new JSONArray();
    }
    
    private static String getLastKey(String key){
        String keys[] = key.split("\\.");
        return keys[keys.length-1];
    }
    
    public static Object getValueJson(Type type, JSONObject jsonStart, String keys, Object valueDefault) throws Exception{
        
        if (jsonStart == null){
            return valueDefault;
        }
        
        if (keys == null || keys.isEmpty()){
            return valueDefault;
        }
        
        // separa em vários kays/indexs
        String listKeys[]= keys.split(";");
        
        boolean flag = false;
        String keySearch = "";
        JSONObject json = null;
        
        // percore todos os indexs
        for(String key : listKeys){
            
            key = key.trim();
                        
            try{
                json = getJson(jsonStart, key);
                keySearch = getLastKey(key);        
            }catch(Exception ex){                
            }

            if (json != null && json.has(keySearch)){
                flag = true;
                break;
            }
            
        }
        
        if (!flag){
            return valueDefault;
        }
             
        Object value = json.get(keySearch);
        
        if (value == null){
            return valueDefault;
        }        
        
        if (type == Type.TEXT){
            if (value.toString().equals("null")){
                return valueDefault;
            }
            return value.toString();
        }
        
        if (type == Type.INTEGER){
            return (int) value;
        }
        
        if (type == Type.DOUBLE){
            if (value instanceof Integer){
                return (int) value;
            }
            return (double) value;
        }
        
        if (type == Type.LONG){
            return (long) value;
        }
        
        if (type == Type.ARRAY_JSON){
            return (JSONArray) value;
        }
        
        throw new Exception("Invalid data type!");
        
    }
    
    public static String convertObjectToString(Object value, String valueDefault){
        if (value == null){
            return valueDefault;
        }
        return (String) value;
    }
    
    public static Integer convertObjectToInteger(Object value, Integer valueDefault){
        if (value == null){
            return valueDefault;
        }
        return (int) value;
    }
        
    
    public static JSONArray convertObjectToJSONArray(Object value){
        JSONArray jSONArray = new JSONArray();
        if (value == null){
            return jSONArray;            
        }
        jSONArray = (JSONArray) value;
        return jSONArray;
    }
    
    public static String getPathUrl(String url) throws Exception{        
        String array[] = url.split("/");
        return array[array.length-1];        
    }
    
}
