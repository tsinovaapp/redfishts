package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MemorySystem {
    
    public static final String TYPE = "MemorySystem";
    public static final String PREFIX = "sys_";
    
    private String urlMemory;
    private String systemUuid;
    private String serverProductSerial;
    
    private Integer memorySizeMB; // [SizeMB, CapacityMiB]
    private String memoryName; // [Name]
    private String memoryDeviceType; // [MemoryDeviceType, DIMMType]
    private String memoryManufacturer; // [Manufacturer]
    private String memoryStatusState; // [Status.State]
    private String memoryStatusHealth; // [Status.Health]
    private String memorySocket; // [SocketLocator, MemoryLocation.Socket]   
    private Integer minimumVoltageVoltsX10; // [MinimumVoltageVoltsX10]
    private Integer maximumFrequencyMHz; // [MaximumFrequencyMHz]
    private String dimmStatus; // [DIMMStatus]
    
    private MemorySystem(Server server, String urlMemory, String systemUuid, String serverProductSerial) throws Exception {
        
        this.urlMemory = urlMemory;
        
        HttpResponse<JsonNode> request = Unirest.get(urlMemory).
                basicAuth(server.getUser(), server.getPassword()).
                asJson();
        
        
        if (request.getBody() == null){
            throw new Exception("No response to " + urlMemory);
        }
        
        
        JSONObject json = request.getBody().getObject();
        
        memorySizeMB = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "SizeMB;CapacityMiB", null), null);
        memoryName = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Name", ""), "");
        memoryDeviceType = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "MemoryDeviceType;DIMMType", ""), "");
        memoryManufacturer = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Manufacturer", ""), "");        
        memorySocket = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "SocketLocator;MemoryLocation.Socket", ""), "");               
        memoryStatusState = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.State", ""), "");        
        memoryStatusHealth = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.Health", ""), "");               
        minimumVoltageVoltsX10 = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "MinimumVoltageVoltsX10", null), null);
        maximumFrequencyMHz = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "MaximumFrequencyMHz", null), null);
        dimmStatus = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "dimmStatus", ""), "");                
        
        this.systemUuid = systemUuid;
        this.serverProductSerial = serverProductSerial;
        
    }
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();        
        json.put(SystemRedfish.PREFIX + "uuid", systemUuid);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "memory_size_mb", memorySizeMB == null ? 0 : memorySizeMB);
        json.put(PREFIX + "memory_name", memoryName);        
        json.put(PREFIX + "memory_device_type", memoryDeviceType);
        json.put(PREFIX + "memory_manufacturer", memoryManufacturer);
        json.put(PREFIX + "memory_socket", memorySocket);  
        json.put(PREFIX + "memory_status_state", memoryStatusState);           
        json.put(PREFIX + "memory_status_health", memoryStatusHealth);           
        json.put(PREFIX + "memory_minimum_voltage_volts_x10", minimumVoltageVoltsX10);   
        json.put(PREFIX + "memory_maximum_frequency_mhz", maximumFrequencyMHz);   
        json.put(PREFIX + "memory_dimm_status", dimmStatus);           
        json.put("type", TYPE);        
        return json;
        
    }
    
//    public String generateIdDoc() throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception{
//        
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        BigInteger hash = new BigInteger(1, md.digest(toJSONObject().toString().getBytes("UTF-8")));        
//        return systemUuid + "-" + Util.getPathUrl(urlMemory) + "-" + hash.toString(16);
//        
//    }
    
    
    public static List<MemorySystem> getListMemorySystem(Server server, String urlSystem,
            String systemUuid, String serverProductSerial) throws Exception{
                
        ArrayList<String> urlsMemorys = getUrlsMemorys(server, urlSystem);
        
        List<MemorySystem> listProcessorSystem = new ArrayList<>();
        
        for(String urlProcessor : urlsMemorys){
            listProcessorSystem.add(new MemorySystem(server, urlProcessor, systemUuid, serverProductSerial));
        }
        
        return listProcessorSystem;
        
    }
    
    
    private static ArrayList<String> getUrlsMemorys(Server server, String urlSystem) throws Exception{
        
        HttpResponse<JsonNode> request = Unirest.get(urlSystem + "Memory/")
                        .basicAuth(server.getUser(), server.getPassword()).asJson();
        
        JSONArray members = request.getBody().getObject().getJSONArray("Members");
        
        ArrayList<String> urlsMemorys = new ArrayList<>();
        
        for (int i = 0; i < members.length(); i++) {
            
            JSONObject member = members.getJSONObject(i);            
            String path = member.getString("@odata.id");       
            urlsMemorys.add(Util.getUrl(server.getUrl(), path));   
            
        }
                
        return urlsMemorys;
        
    }    

    public String getUrlMemory() {
        return urlMemory;
    }

    public void setUrlMemory(String urlMemory) {
        this.urlMemory = urlMemory;
    }

    public String getSystemUuid() {
        return systemUuid;
    }

    public void setSystemUuid(String systemUuid) {
        this.systemUuid = systemUuid;
    }

    public Integer getMemorySizeMB() {
        return memorySizeMB;
    }

    public void setMemorySizeMB(Integer memorySizeMB) {
        this.memorySizeMB = memorySizeMB;
    }

    public String getMemoryName() {
        return memoryName;
    }

    public void setMemoryName(String memoryName) {
        this.memoryName = memoryName;
    }

    public String getMemoryDeviceType() {
        return memoryDeviceType;
    }

    public void setMemoryDeviceType(String memoryDeviceType) {
        this.memoryDeviceType = memoryDeviceType;
    }

    public String getMemoryManufacturer() {
        return memoryManufacturer;
    }

    public void setMemoryManufacturer(String memoryManufacturer) {
        this.memoryManufacturer = memoryManufacturer;
    }

    public String getMemoryStatusState() {
        return memoryStatusState;
    }

    public void setMemoryStatusState(String memoryStatusState) {
        this.memoryStatusState = memoryStatusState;
    }

    public String getMemoryStatusHealth() {
        return memoryStatusHealth;
    }

    public void setMemoryStatusHealth(String memoryStatusHealth) {
        this.memoryStatusHealth = memoryStatusHealth;
    }

    public String getMemorySocket() {
        return memorySocket;
    }

    public void setMemorySocket(String memorySocket) {
        this.memorySocket = memorySocket;
    }    

    public Integer getMinimumVoltageVoltsX10() {
        return minimumVoltageVoltsX10;
    }

    public void setMinimumVoltageVoltsX10(Integer minimumVoltageVoltsX10) {
        this.minimumVoltageVoltsX10 = minimumVoltageVoltsX10;
    }

    public Integer getMaximumFrequencyMHz() {
        return maximumFrequencyMHz;
    }

    public void setMaximumFrequencyMHz(Integer maximumFrequencyMHz) {
        this.maximumFrequencyMHz = maximumFrequencyMHz;
    }

    public String getDimmStatus() {
        return dimmStatus;
    }

    public void setDimmStatus(String dimmStatus) {
        this.dimmStatus = dimmStatus;
    }

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }    
    
}
