package br.com.tsinova.redfishts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SystemBoot {
    
    private String bootSourceOverrideEnabled;
    private JSONArray bootSourceOverrideSupported;
    private JSONArray uefiTargetBootSourceOverrideSupported;

    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();
        json.put("boot_source_override_enabled", bootSourceOverrideEnabled);
        json.put("boot_source_override_supported", bootSourceOverrideSupported);
        json.put("uefi_target_boot_source_override_supported", uefiTargetBootSourceOverrideSupported);                
        return json;
        
    }    
    
    public SystemBoot(JSONObject jsonStart) throws Exception {        
        bootSourceOverrideEnabled = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "Boot.BootSourceOverrideEnabled", ""), "");
        bootSourceOverrideSupported = Util.convertObjectToJSONArray(Util.getValueJson(Type.ARRAY_JSON, jsonStart, "Boot.BootSourceOverrideSupported", Util.getJSONArrayEmpty()));
        uefiTargetBootSourceOverrideSupported = Util.convertObjectToJSONArray(Util.getValueJson(Type.ARRAY_JSON, jsonStart, "Boot.UefiTargetBootSourceOverrideSupported", Util.getJSONArrayEmpty()));
        
    }
    

    public String getBootSourceOverrideEnabled() {
        return bootSourceOverrideEnabled;
    }

    public void setBootSourceOverrideEnabled(String bootSourceOverrideEnabled) {
        this.bootSourceOverrideEnabled = bootSourceOverrideEnabled;
    }

    public JSONArray getBootSourceOverrideSupported() {
        return bootSourceOverrideSupported;
    }

    public void setBootSourceOverrideSupported(JSONArray bootSourceOverrideSupported) {
        this.bootSourceOverrideSupported = bootSourceOverrideSupported;
    }

    public JSONArray getUefiTargetBootSourceOverrideSupported() {
        return uefiTargetBootSourceOverrideSupported;
    }

    public void setUefiTargetBootSourceOverrideSupported(JSONArray uefiTargetBootSourceOverrideSupported) {
        this.uefiTargetBootSourceOverrideSupported = uefiTargetBootSourceOverrideSupported;
    }    

    @Override
    public String toString() {
        return "SystemBoot{" + "bootSourceOverrideEnabled=" + bootSourceOverrideEnabled + ", bootSourceOverrideSupported=" + bootSourceOverrideSupported + ", uefiTargetBootSourceOverrideSupported=" + uefiTargetBootSourceOverrideSupported + '}';
    }    
    
}
