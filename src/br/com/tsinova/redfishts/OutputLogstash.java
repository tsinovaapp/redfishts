package br.com.tsinova.redfishts;

import org.json.JSONException;
import org.json.JSONObject;

public class OutputLogstash extends Output{

    public OutputLogstash(JSONObject json) throws JSONException {
        host = json.getJSONObject("logstash").getString("host");
        port = json.getJSONObject("logstash").getInt("port");
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
}
