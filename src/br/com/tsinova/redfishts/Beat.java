package br.com.tsinova.redfishts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Beat {
    
    private String name;
    private String version;
    private JSONArray tags;  

    public Beat(JSONObject json) throws JSONException {
        name = json.getString("name");
        version = json.getString("version");
        tags = json.getJSONArray("tags");
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public JSONArray getTags() {
        return tags;
    }

    public void setTags(JSONArray tags) {
        this.tags = tags;
    }

}
