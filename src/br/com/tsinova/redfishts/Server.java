package br.com.tsinova.redfishts;

import org.json.JSONException;
import org.json.JSONObject;

public class Server {
    
    private int interval;
    private String url;
    private String path;
    private String user;
    private String password;
        
    public Server(JSONObject json) throws JSONException {
        interval = json.getInt("interval");
        url = json.getString("url");
        path = json.getString("path");
        user = json.getString("user");
        password = json.getString("password");
    }

    public String getIpHost(){        
        String regex = "https://(\\d{1,}.\\d{1,}.\\d{1,}.\\d{1,})/*";        
        if (!url.matches(regex)){
            return "";
        }        
        return url.replaceAll(regex, "$1").trim();                
    }
    
    public Server() {
    }    

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }   
    
    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    
    
}
