package br.com.tsinova.redfishts;

import io.github.openunirest.http.Unirest;
import io.github.openunirest.http.options.Options;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;

public class ReadSend extends Thread {

    private final Server server;
    private final List<Output> listOutput;
    private boolean run;
    private final Beat beat;

    public ReadSend(Server server, List<Output> listOutput, Beat beat) {
        this.server = server;
        this.listOutput = listOutput;
        this.run = true;
        this.beat = beat;
    }

    public void close() {
        this.run = false;
    }

    private JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    public void setConfigUnirest() throws Exception {
        Options.enableCookieManagement(false);

        SSLContext sslcontext = SSLContexts.custom().
                loadTrustMaterial(null, new TrustSelfSignedStrategy()).
                build();

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        CloseableHttpClient httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .build();

        Unirest.setTimeouts(3000, 1500);
        Unirest.setDefaultHeader("Accept", "application/json");
        Unirest.setHttpClient(httpclient);

    }

    @Override
    public void run() {

        while (run) {

            try {

                System.out.println("Searching information " + server.getUrl());

                setConfigUnirest();

                List<SystemRedfish> listSystemRedfish;

                try {
                    listSystemRedfish = SystemRedfish.getListSystemRedfish(server);

                } catch (Exception ex) {
                    // Intervalo de espera
                    System.err.println("Error: Could not fetch information from " + server.getUrl());
                    try {
                        Thread.sleep(server.getInterval() * 1000);
                    } catch (InterruptedException ex2) {
                    }
                    continue;

                }

                List<ChassisRedfish> listChassisRedfishs;
                try {
                    listChassisRedfishs = ChassisRedfish.getListChassis(server);

                } catch (Exception ex) {
                    // Intervalo de espera
                    System.err.println("Error: Could not fetch information from " + server.getUrl());
                    try {
                        Thread.sleep(server.getInterval() * 1000);
                    } catch (InterruptedException ex2) {
                    }
                    continue;

                }

                System.out.println("Search performed on " + server.getUrl() + " successfully");

                // Seta outros atributos, atributos que qualquer documento tem
                JSONObject jsonDefault = new JSONObject();

                try {

                    jsonDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                    JSONObject device = new JSONObject();
                    device.put("ip", server.getIpHost());

                    jsonDefault.put("device", device);

                    jsonDefault.put("host", server.getUrl());

                    JSONObject metadataJSON = new JSONObject();
                    metadataJSON.put("beat", beat.getName());
                    metadataJSON.put("version", beat.getVersion());
                    jsonDefault.put("@metadata", metadataJSON);

                    JSONObject beatJSON = new JSONObject();
                    beatJSON.put("name", beat.getName());
                    beatJSON.put("version", beat.getVersion());
                    jsonDefault.put("beat", beatJSON);

                    jsonDefault.put("tags", beat.getTags());

                } catch (JSONException ex) {
                }

                System.out.println("Sending data ... ");

                // envia os dados do sistema
                for (SystemRedfish systemRedfish : listSystemRedfish) {

                    for (Output out : listOutput) {

                        // envia as informações do sistema
                        JSONObject jsonSystemRedfishForSocket = getJson(jsonDefault, systemRedfish.toJSONObject());

                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(jsonSystemRedfishForSocket.toString());
                            os.flush();
                        }

                        // envia as informações de potência
                        JSONObject jsonPowerSystemForSocket = getJson(jsonDefault, systemRedfish.getPowerControlSystem().toJSONObject());

                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(jsonPowerSystemForSocket.toString());
                            os.flush();
                        }

                        // envia as informações de potência duplicado
                        jsonPowerSystemForSocket.put("type", "Latest" + PowerControlSystem.TYPE);
                        jsonPowerSystemForSocket.put(Util.NAME_DOC_ID, "server_" + systemRedfish.getPowerControlSystem().getServerProductSerial()
                                + "_latest_" + PowerControlSystem.TYPE.toLowerCase());
                        try (Socket socket = new Socket(out.host, out.port);
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(jsonPowerSystemForSocket.toString());
                            os.flush();
                        }                        

                        // envia os processadores
                        for (ProcessorSystem processorSystem : systemRedfish.getListProcessors()) {

                            JSONObject jsonProcessorSystemForSocket = getJson(jsonDefault, processorSystem.toJSONObject());
                            String id = "server_" + processorSystem.getServerProductSerial()
                                    + "_processor_" + processorSystem.getProcessorModel() + "_socket_" + processorSystem.getProcessorSocket();
                            jsonProcessorSystemForSocket.put(Util.NAME_DOC_ID, id);
                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonProcessorSystemForSocket.toString());
                                os.flush();
                            }

                        }

                        // envia as memórias
                        for (MemorySystem memorySystem : systemRedfish.getListMemorys()) {

                            JSONObject jsonMemorySystemForSocket = getJson(jsonDefault, memorySystem.toJSONObject());
                            String id = "server_" + memorySystem.getServerProductSerial()
                                    + "_memory_" + memorySystem.getMemoryName() + "_socket_" + memorySystem.getMemorySocket();
                            jsonMemorySystemForSocket.put(Util.NAME_DOC_ID, id);

                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonMemorySystemForSocket.toString());
                                os.flush();
                            }

                        }

                        // envia os adaptadores de rede (portas)
                        for (NetworkAdapterSystem networkAdapterSystem : systemRedfish.getListNetworkAdapter()) {

                            JSONObject jsonNetworkAdapterSystemForSocket = getJson(jsonDefault, networkAdapterSystem.toJSONObject());

                            String id = "server_" + networkAdapterSystem.getServerProductSerial()
                                    + "_networkadapter_" + networkAdapterSystem.getMacAddress();
                            jsonNetworkAdapterSystemForSocket.put(Util.NAME_DOC_ID, id);

                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonNetworkAdapterSystemForSocket.toString());
                                os.flush();
                            }

                        }

                        // envia as garantias
                        for (Warranty warranty : systemRedfish.getListWarranty()) {

                            JSONObject jsonWrrantyForSocket = getJson(jsonDefault, warranty.toJSONObject());

                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonWrrantyForSocket.toString());
                                os.flush();
                            }

                        }

                    }

                }

                // envia os dados do chassi
                for (ChassisRedfish chassisRedfish : listChassisRedfishs) {

                    for (Output out : listOutput) {

                        // envia as ventoinhas
                        for (FanChassi fanChassi : chassisRedfish.getListFans()) {

                            JSONObject jsonFanChassiForSocket = getJson(jsonDefault, fanChassi.toJSONObject());

                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonFanChassiForSocket.toString());
                                os.flush();
                            }

                        }

                        // envia os sensores
                        for (SensorTemperatureChassi sensorChassi : chassisRedfish.getListSensorsTemperature()) {

                            JSONObject jsonSensorTemperatureForSocket = getJson(jsonDefault, sensorChassi.toJSONObject());

                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonSensorTemperatureForSocket.toString());
                                os.flush();
                            }

                        }

                    }

                }

                System.out.println("Data sent!");

                Unirest.shutdown();

                System.out.println("Waiting time to search again ...");

            } catch (Exception ex) {
                System.err.println("Error: Failed to forward data " + server.getUrl());
                ex.printStackTrace();

            }

            // Intervalo de espera
            try {
                Thread.sleep(server.getInterval() * 1000);
            } catch (InterruptedException ex) {
            }

        }

    }

}
