package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ChassisRedfish {
    
    public static final String TYPE = "Chassis";
    
    private String codChassi;
    private String serverProductSerial;
    private List<FanChassi> listFans;   
    private List<SensorTemperatureChassi> listSensorsTemperature;   
       
    private ChassisRedfish(Server server, String urlChassi) throws Exception {   
        codChassi = Util.getPathUrl(urlChassi);
        
        HttpResponse<JsonNode> request = Unirest.get(urlChassi).
                basicAuth(server.getUser(), server.getPassword()).
                asJson();

        if (request.getBody() == null) {
            throw new Exception("No response to " + urlChassi);
        }

        JSONObject json = request.getBody().getObject();
        
        serverProductSerial = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "SerialNumber", ""), "");                                
        listFans = FanChassi.getListFans(server, urlChassi, codChassi, serverProductSerial);    
        listSensorsTemperature = SensorTemperatureChassi.getListSensorsTemperature(server, urlChassi, codChassi, serverProductSerial);
        
    }
    
    public static List<ChassisRedfish> getListChassis(Server server) throws Exception{
        
        String urlChassi = getUrlChassis(server);
        ArrayList<String> urlsChassis = getUrlsChassis(server, urlChassi);
        
        List<ChassisRedfish> listSystemRedfish = new ArrayList<>();
        
        for(String urlCha : urlsChassis){
            listSystemRedfish.add(new ChassisRedfish(server, urlCha));
        }
        
        return listSystemRedfish;
        
    }
    
    /**
     * Retorna uma lista de url para cada chassi
     * @param server
     * @param urlChassis
     * @return
     * @throws Exception 
     */
    private static ArrayList<String> getUrlsChassis(Server server, String urlChassis) throws Exception{
        
        HttpResponse<JsonNode> request = Unirest.get(urlChassis)
                        .basicAuth(server.getUser(), server.getPassword()).asJson();
        
        JSONArray members = request.getBody().getObject().getJSONArray("Members");
        
        ArrayList<String> urlsChassis = new ArrayList<>();
        
        for (int i = 0; i < members.length(); i++) {
            
            JSONObject member = members.getJSONObject(i);            
            String path = member.getString("@odata.id");       
            urlsChassis.add(Util.getUrl(server.getUrl(), path));   
            
        }
                
        return urlsChassis;
        
    }
    
    
    /**
     * Coleta a url para buscar todas as urls de chassis
     * @param server
     * @return
     * @throws Exception 
     */
    private static String getUrlChassis(Server server) throws Exception{
        
        // exemplo: https://192.168.0.1/redfish/v1/
        String url = Util.getUrl(server.getUrl(), server.getPath());
        
        HttpResponse<JsonNode> request = Unirest.get(url)
                        .basicAuth(server.getUser(), server.getPassword()).asJson();
        
        JSONObject system = request.getBody().getObject().getJSONObject(TYPE);
        
        String path = system.getString("@odata.id");
        
        return Util.getUrl(server.getUrl(), path);        
        
    }

    public List<FanChassi> getListFans() {
        return listFans;
    }

    public void setListFans(List<FanChassi> listFans) {
        this.listFans = listFans;
    }    

    public String getCodChassi() {
        return codChassi;
    }

    public void setCodChassi(String codChassi) {
        this.codChassi = codChassi;
    }    

    public List<SensorTemperatureChassi> getListSensorsTemperature() {
        return listSensorsTemperature;
    }

    public void setListSensorsTemperature(List<SensorTemperatureChassi> listSensorsTemperature) {
        this.listSensorsTemperature = listSensorsTemperature;
    } 

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }    
    
}
