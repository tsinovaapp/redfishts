package br.com.tsinova.redfishts;

import org.json.JSONException;
import org.json.JSONObject;

public class SystemProcessorSummary {
    
    private Integer count;
    private String model;    
    private String statusState;
    private String statusHealth;
    private String statusHealthrollup;
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();
        json.put("count", count == null ? 0 : count);
        json.put("model", model);
        json.put("status_state", statusState);
        json.put("status_health", statusHealth);
        json.put("status_healthrollup", statusHealthrollup);                
        return json;
        
    } 

    public SystemProcessorSummary(JSONObject jsonStart) throws Exception {                
        count = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, jsonStart, "ProcessorSummary.Count", null), null);
        model = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "ProcessorSummary.Model", ""), "");        
        statusState = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "ProcessorSummary.Status.State", ""), "");
        statusHealth = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "ProcessorSummary.Status.Health", ""), "");
        statusHealthrollup = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonStart, "ProcessorSummary.Status.HealthRollUp", ""), "");
                
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getStatusState() {
        return statusState;
    }

    public void setStatusState(String statusState) {
        this.statusState = statusState;
    }

    public String getStatusHealth() {
        return statusHealth;
    }

    public void setStatusHealth(String statusHealth) {
        this.statusHealth = statusHealth;
    }

    public String getStatusHealthrollup() {
        return statusHealthrollup;
    }

    public void setStatusHealthrollup(String statusHealthrollup) {
        this.statusHealthrollup = statusHealthrollup;
    }

    @Override
    public String toString() {
        return "SystemProcessorSummary{" + "count=" + count + ", model=" + model + ", statusState=" + statusState + ", statusHealth=" + statusHealth + ", statusHealthrollup=" + statusHealthrollup + '}';
    }  
   
    
}
