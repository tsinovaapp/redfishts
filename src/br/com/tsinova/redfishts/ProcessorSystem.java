package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProcessorSystem {
    
    public static final String TYPE = "ProcessorSystem";
    public static final String PREFIX = "sys_";
    
    private String processorName;
    private String processorSocket;
    private String processorType;
    private String processorArchitecture;
    private Integer processorTotalCores;
    private Integer processorTotalThreads;
    private String processorModel;
    private String processorManufacturer;
    private String processorInstructionSet;
    private Integer processorMaxSpeedMhz;
    private String processor_status_state;
    private String processor_status_health;
    private String systemUuid;
    private String serverProductSerial;
    private String urlProcessor;
    
    private ProcessorSystem(Server server, String urlProcessor, String systemUuid, String serverProductSerial) throws Exception {
        
        this.urlProcessor = urlProcessor;
        
        HttpResponse<JsonNode> request = Unirest.get(urlProcessor).
                basicAuth(server.getUser(), server.getPassword()).
                asJson();
        
        
        if (request.getBody() == null){
            throw new Exception("No response to " + urlProcessor);
        }
        
        
        JSONObject json = request.getBody().getObject();
        
        processorName = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Name", ""), "");
        processorSocket = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Socket", ""), "");
        processorType = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "ProcessorType", ""), "");
        processorArchitecture = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "ProcessorArchitecture", ""), "");
        processorTotalCores = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "TotalCores", null), null);
        processorTotalThreads = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "TotalThreads", null), null);
        processorModel = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Model", ""), "");        
        processorManufacturer = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Manufacturer", ""), "");
        processorInstructionSet = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "InstructionSet", ""), "");
        processorMaxSpeedMhz = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "MaxSpeedMHz", null), null);                
        processor_status_state = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.State", ""), "");        
        processor_status_health = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.Health", ""), "");        
        this.systemUuid = systemUuid;
        this.serverProductSerial = serverProductSerial;
        
    }
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();        
        json.put(SystemRedfish.PREFIX + "uuid", systemUuid);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "processor_name", processorName);
        json.put(PREFIX + "processor_socket", processorSocket);        
        json.put(PREFIX + "processor_type", processorType);
        json.put(PREFIX + "processor_architecture", processorArchitecture);
        json.put(PREFIX + "processor_total_cores", processorTotalCores);        
        json.put(PREFIX + "processor_total_threads", processorTotalThreads);
        json.put(PREFIX + "processor_model", processorModel);
        json.put(PREFIX + "processor_manufacturer", processorManufacturer);
        json.put(PREFIX + "processor_instruction_set", processorInstructionSet);
        json.put(PREFIX + "processor_max_speed_mhz", processorMaxSpeedMhz);   
        json.put(PREFIX + "processor_status_state", processor_status_state);           
        json.put(PREFIX + "processor_status_health", processor_status_health);   
        json.put("type", TYPE);        
        return json;
        
    }
    
//    public String generateIdDoc() throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception{
//        
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        BigInteger hash = new BigInteger(1, md.digest(toJSONObject().toString().getBytes("UTF-8")));        
//        return systemUuid + "-" + Util.getPathUrl(urlProcessor) + "-" + hash.toString(16);
//        
//    }    
    
    
    public static List<ProcessorSystem> getListProcessorsSystem(Server server, String urlSystem, 
            String systemUuid, String serverProductSerial) throws Exception{
                
        ArrayList<String> urlsProcessors = getUrlsProcessors(server, urlSystem);
        
        List<ProcessorSystem> listProcessorSystem = new ArrayList<>();
        
        for(String urlProcessor : urlsProcessors){
            listProcessorSystem.add(new ProcessorSystem(server, urlProcessor, systemUuid, serverProductSerial));
        }
        
        return listProcessorSystem;
        
    }
    
    
    private static ArrayList<String> getUrlsProcessors(Server server, String urlSystem) throws Exception{
        
        HttpResponse<JsonNode> request = Unirest.get(urlSystem + "Processors/")
                        .basicAuth(server.getUser(), server.getPassword()).asJson();
        
        JSONArray members = request.getBody().getObject().getJSONArray("Members");
        
        ArrayList<String> urlsProcessors = new ArrayList<>();
        
        for (int i = 0; i < members.length(); i++) {
            
            JSONObject member = members.getJSONObject(i);            
            String path = member.getString("@odata.id");       
            urlsProcessors.add(Util.getUrl(server.getUrl(), path));   
            
        }
                
        return urlsProcessors;
        
    }

    public String getProcessorName() {
        return processorName;
    }

    public void setProcessorName(String processorName) {
        this.processorName = processorName;
    }

    public String getProcessorSocket() {
        return processorSocket;
    }

    public void setProcessorSocket(String processorSocket) {
        this.processorSocket = processorSocket;
    }

    public String getProcessorType() {
        return processorType;
    }

    public void setProcessorType(String processorType) {
        this.processorType = processorType;
    }

    public String getProcessorArchitecture() {
        return processorArchitecture;
    }

    public void setProcessorArchitecture(String processorArchitecture) {
        this.processorArchitecture = processorArchitecture;
    }

    public Integer getProcessorTotalCores() {
        return processorTotalCores;
    }

    public void setProcessorTotalCores(Integer processorTotalCores) {
        this.processorTotalCores = processorTotalCores;
    }

    public Integer getProcessorTotalThreads() {
        return processorTotalThreads;
    }

    public void setProcessorTotalThreads(Integer processorTotalThreads) {
        this.processorTotalThreads = processorTotalThreads;
    }

    public String getProcessorModel() {
        return processorModel;
    }

    public void setProcessorModel(String processorModel) {
        this.processorModel = processorModel;
    }

    public String getProcessorManufacturer() {
        return processorManufacturer;
    }

    public void setProcessorManufacturer(String processorManufacturer) {
        this.processorManufacturer = processorManufacturer;
    }

    public String getProcessorInstructionSet() {
        return processorInstructionSet;
    }

    public void setProcessorInstructionSet(String processorInstructionSet) {
        this.processorInstructionSet = processorInstructionSet;
    }

    public Integer getProcessorMaxSpeedMhz() {
        return processorMaxSpeedMhz;
    }

    public void setProcessorMaxSpeedMhz(Integer processorMaxSpeedMhz) {
        this.processorMaxSpeedMhz = processorMaxSpeedMhz;
    }

    public String getProcessor_status_state() {
        return processor_status_state;
    }

    public void setProcessor_status_state(String processor_status_state) {
        this.processor_status_state = processor_status_state;
    }

    public String getProcessor_status_health() {
        return processor_status_health;
    }

    public void setProcessor_status_health(String processor_status_health) {
        this.processor_status_health = processor_status_health;
    }

    public String getSystemUuid() {
        return systemUuid;
    }

    public void setSystemUuid(String systemUuid) {
        this.systemUuid = systemUuid;
    }

    public String getUrlProcessor() {
        return urlProcessor;
    }

    public void setUrlProcessor(String urlProcessor) {
        this.urlProcessor = urlProcessor;
    }    

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }    
    
}
