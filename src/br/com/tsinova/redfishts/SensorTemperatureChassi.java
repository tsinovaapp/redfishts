package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class SensorTemperatureChassi {

    public static final String TYPE = "Sensor";
    public static final String PREFIX = "sensor_";

    private String id;
    private String serverProductSerial;
    
    private String name;
    private String location;
    private Integer locationX;
    private Integer locationY;
    private String health;
    private String state;
    private Integer reading;
    private String readingUnits;
    private Integer lowerThresholdCritical;
    private Integer lowerThresholdNonCritical;
    private String thresholds;
    private Integer number;

    private SensorTemperatureChassi(Server server, String codChassi, JSONObject json, String serverProductSerial) throws Exception {
        name = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Name", ""), "");
        location = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "PhysicalContext", ""), "");
        locationX = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "Oem.Hp.LocationXmm", null), null);
        locationY = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "Oem.Hp.LocationYmm", null), null);
        health = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.Health", ""), "");
        state = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.State", ""), "");
        reading = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "CurrentReading", null), null);
        readingUnits = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Units", ""), "");
        lowerThresholdCritical = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "LowerThresholdCritical", null), null);
        lowerThresholdNonCritical = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "LowerThresholdNonCritical", null), null);
        number = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "Number", null), null);
        thresholds = "Caution: " + (lowerThresholdNonCritical == 0 ? "N/A;"
                : lowerThresholdNonCritical + readingUnits.substring(0, 1) + ";")
                + " Critical: " + (lowerThresholdCritical == 0 ? "N/A" : lowerThresholdCritical + readingUnits.substring(0, 1));
        id = "server_" + serverProductSerial + "_chasssi_" + codChassi + "_sensor_" + name + "_" + number;
        this.serverProductSerial = serverProductSerial;
        //codSensor = server.getIpHost() + "-" + codChassi + "-" + name + "-" + number;
    }

    public JSONObject toJSONObject() throws Exception {

        JSONObject json = new JSONObject();
        //json.put(Util.NAME_DOC_ID, generateIdDoc());
        json.put(Util.NAME_DOC_ID, id);
        json.put(PREFIX + "name", name);
        json.put(PREFIX + "location", location);
        json.put(PREFIX + "location_x", locationX);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "location_y", locationY);
        json.put(PREFIX + "health", health);
        json.put(PREFIX + "state", state);
        json.put(PREFIX + "reading", reading);
        json.put(PREFIX + "reading_units", readingUnits);        
        json.put(PREFIX + "lower_threshold_critical", lowerThresholdCritical);
        json.put(PREFIX + "lower_threshold_non_critical", lowerThresholdNonCritical);
        json.put(PREFIX + "number", number);
        json.put(PREFIX + "thresholds", thresholds);                
        json.put("type", TYPE);
        return json;

    }

//    private String generateIdDoc() throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception {
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        BigInteger hash = new BigInteger(1, md.digest(id.getBytes("UTF-8")));
//        return hash.toString(16);
//    }

    public static List<SensorTemperatureChassi> getListSensorsTemperature(Server server, String urlChassi, 
            String codChassi, String serverProductSerial) throws Exception {

        HttpResponse<JsonNode> request = Unirest.get(urlChassi + "Thermal/").
                basicAuth(server.getUser(), server.getPassword()).
                asJson();

        if (request.getBody() == null) {
            throw new Exception("No response to " + urlChassi + "Thermal/");
        }

        JSONObject json = request.getBody().getObject();

        JSONArray sensorsTemperatures = json.getJSONArray("Temperatures");

        List<SensorTemperatureChassi> listSensorsTemperature = new ArrayList<>();

        for (int i = 0; i < sensorsTemperatures.length(); i++) {
            listSensorsTemperature.add(new SensorTemperatureChassi(server, codChassi, 
                    sensorsTemperatures.getJSONObject(i), serverProductSerial));
        }

        return listSensorsTemperature;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getReading() {
        return reading;
    }

    public void setReading(Integer reading) {
        this.reading = reading;
    }

    public String getReadingUnits() {
        return readingUnits;
    }

    public void setReadingUnits(String readingUnits) {
        this.readingUnits = readingUnits;
    }

    public Integer getLowerThresholdCritical() {
        return lowerThresholdCritical;
    }

    public void setLowerThresholdCritical(Integer lowerThresholdCritical) {
        this.lowerThresholdCritical = lowerThresholdCritical;
    }

    public Integer getLowerThresholdNonCritical() {
        return lowerThresholdNonCritical;
    }

    public void setLowerThresholdNonCritical(Integer lowerThresholdNonCritical) {
        this.lowerThresholdNonCritical = lowerThresholdNonCritical;
    }

    public String getThresholds() {
        return thresholds;
    }

    public void setThresholds(String thresholds) {
        this.thresholds = thresholds;
    }

    public Integer getLocationX() {
        return locationX;
    }

    public void setLocationX(Integer locationX) {
        this.locationX = locationX;
    }

    public Integer getLocationY() {
        return locationY;
    }

    public void setLocationY(Integer locationY) {
        this.locationY = locationY;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }    
    
}
