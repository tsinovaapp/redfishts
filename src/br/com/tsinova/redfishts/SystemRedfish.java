package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SystemRedfish {
    
    public static final String TYPE = "Systems";
    public static final String PREFIX = "sys_";
    
    private String systemUuid;
    private String systemBiosVersion;
    private String systemDescription;
    private String systemHostName;
    private String systemModel;
    private String systemName;
    private String systemType;
    private String systemManufacturer;
    private String systemSerialNumber;
    private String serverProductSerial;
    private String systemSku;
    private String systemPower;
    private String systemPowerState;
    private JSONArray systemHostMacaddress;
    
    private SystemBoot systemBoot;
    private SystemProcessorSummary systemProcessorSummary;
    private SystemMemorySummary systemMemorySummary;
    private PowerControlSystem powerControlSystem;
            
    private List<ProcessorSystem> listProcessors;
    private List<MemorySystem> listMemorys;
    private List<Warranty> listWarranty;
    private List<NetworkAdapterSystem> listNetworkAdapter;
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();
        json.put(Util.NAME_DOC_ID, systemUuid);
        json.put(PREFIX + "uuid", systemUuid);
        json.put(PREFIX + "bios_version", systemBiosVersion);
        json.put(PREFIX + "description", systemDescription);        
        json.put(PREFIX + "host_name", systemHostName);
        json.put(PREFIX + "model", systemModel);
        json.put(PREFIX + "name", systemName);        
        json.put(PREFIX + "type", systemType);
        json.put(PREFIX + "manufacturer", systemManufacturer);
        json.put(PREFIX + "serial_number", systemSerialNumber);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "sku", systemSku);
        json.put(PREFIX + "power", systemPower);
        json.put(PREFIX + "power_state", systemPowerState);   
        json.put(PREFIX + "host_macaddress", systemHostMacaddress);           
        json.put(PREFIX + "boot", systemBoot.toJSONObject());   
        json.put(PREFIX + "processor_summary", systemProcessorSummary.toJSONObject());   
        json.put(PREFIX + "memory_summary", systemMemorySummary.toJSONObject());                   
        json.put(PREFIX + "power_summary", powerControlSystem.toJSONObjectSummary());    
        json.put("type", TYPE);        
        return json;
        
    }

    public List<Warranty> getListWarranty() {
        return listWarranty;
    }

    public void setListWarranty(List<Warranty> listWarranty) {
        this.listWarranty = listWarranty;
    }    

    public List<NetworkAdapterSystem> getListNetworkAdapter() {
        return listNetworkAdapter;
    }

    public void setListNetworkAdapter(List<NetworkAdapterSystem> listNetworkAdapter) {
        this.listNetworkAdapter = listNetworkAdapter;
    }
    
    private SystemRedfish(Server server, String urlSystem) throws Exception {
        
        HttpResponse<JsonNode> request = Unirest.get(urlSystem).
                basicAuth(server.getUser(), server.getPassword()).
                asJson();
        
        
        if (request.getBody() == null){
            throw new Exception("No response to " + urlSystem);
        }
        
        
        JSONObject json = request.getBody().getObject();
        
        systemUuid = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "UUID", ""), "");
        systemBiosVersion = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "BiosVersion", ""), "");
        systemDescription = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Description", ""), "");
        systemHostName = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "HostName", ""), "");
        systemModel = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Model", ""), "");
        systemName = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Name", ""), "");
        systemType = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "SystemType", ""), "");        
        systemManufacturer = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Manufacturer", ""), "");        
        systemSerialNumber = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "SerialNumber", ""), "");        
        serverProductSerial = systemSerialNumber;
        systemSku = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "SKU", ""), "");
        systemPower = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Power", ""), "");
        systemPowerState = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "PowerState", ""), "");                
        systemHostMacaddress = Util.convertObjectToJSONArray(Util.getValueJson(Type.ARRAY_JSON, json, "HostCorrelation.HostMACAddress", Util.getJSONArrayEmpty()));        
        
        systemBoot = new SystemBoot(json);
        systemProcessorSummary = new SystemProcessorSummary(json);
        systemMemorySummary = new SystemMemorySummary(json);  
        powerControlSystem = new PowerControlSystem(server, urlSystem, systemUuid, serverProductSerial);
        
        listProcessors = ProcessorSystem.getListProcessorsSystem(server, urlSystem, systemUuid, serverProductSerial);
        listMemorys = MemorySystem.getListMemorySystem(server, urlSystem, systemUuid, serverProductSerial);
        listNetworkAdapter = NetworkAdapterSystem.getListNetworkAdapterSystem(server, urlSystem, systemUuid, serverProductSerial);        
        try{
            listWarranty = Warranty.getListWarranty(systemSerialNumber, systemUuid, serverProductSerial);
        }catch(Exception ex){
            listWarranty = new ArrayList<>();
            System.out.println("Falha ao buscar os dados de garantia do servidor " + systemModel);
            ex.printStackTrace();
        }
        
        
        
    }
    
    public static List<SystemRedfish> getListSystemRedfish(Server server) throws Exception{
        
        String urlSystems = getUrlSystems(server);
        ArrayList<String> urlsSystems = getUrlsSystems(server, urlSystems);
        
        List<SystemRedfish> listSystemRedfish = new ArrayList<>();
        
        for(String urlSystem : urlsSystems){
            listSystemRedfish.add(new SystemRedfish(server, urlSystem));
        }
        
        return listSystemRedfish;
        
    }
    
    /**
     * Retorna uma lista de url para cada sistema
     * @param server
     * @param urlSystems
     * @return
     * @throws Exception 
     */
    private static ArrayList<String> getUrlsSystems(Server server, String urlSystems) throws Exception{
        
        HttpResponse<JsonNode> request = Unirest.get(urlSystems)
                        .basicAuth(server.getUser(), server.getPassword()).asJson();
        
        JSONArray members = request.getBody().getObject().getJSONArray("Members");
        
        ArrayList<String> urlsSystems = new ArrayList<>();
        
        for (int i = 0; i < members.length(); i++) {
            
            JSONObject member = members.getJSONObject(i);            
            String path = member.getString("@odata.id");       
            urlsSystems.add(Util.getUrl(server.getUrl(), path));   
            
        }
                
        return urlsSystems;
        
    }
    
    
    /**
     * Coleta a url para buscar todas as urls de sistemas
     * @param server
     * @return
     * @throws Exception 
     */
    private static String getUrlSystems(Server server) throws Exception{
        
        // exemplo: https://192.168.0.1/redfish/v1/
        String url = Util.getUrl(server.getUrl(), server.getPath());
        
        HttpResponse<JsonNode> request = Unirest.get(url)
                        .basicAuth(server.getUser(), server.getPassword()).asJson();
        
        JSONObject system = request.getBody().getObject().getJSONObject(TYPE);
        
        String path = system.getString("@odata.id");
        
        return Util.getUrl(server.getUrl(), path);        
        
    }

    @Override
    public String toString() {
        return "SystemRedfish{" + "systemUuid=" + systemUuid + ", systemBiosVersion=" + systemBiosVersion + ", systemDescription=" + systemDescription + ", systemHostName=" + systemHostName + ", systemModel=" + systemModel + ", systemName=" + systemName + ", systemType=" + systemType + ", systemSerialNumber=" + systemSerialNumber + ", systemSku=" + systemSku + ", systemPower=" + systemPower + ", systemPowerState=" + systemPowerState + ", systemHostMacaddress=" + systemHostMacaddress + ", systemBoot=" + systemBoot + ", systemProcessorSummary=" + systemProcessorSummary + ", systemMemorySummary=" + systemMemorySummary + '}';
    }    

    public String getSystemUuid() {
        return systemUuid;
    }

    public void setSystemUuid(String systemUuid) {
        this.systemUuid = systemUuid;
    }

    public String getSystemBiosVersion() {
        return systemBiosVersion;
    }

    public void setSystemBiosVersion(String systemBiosVersion) {
        this.systemBiosVersion = systemBiosVersion;
    }

    public String getSystemDescription() {
        return systemDescription;
    }

    public void setSystemDescription(String systemDescription) {
        this.systemDescription = systemDescription;
    }

    public String getSystemHostName() {
        return systemHostName;
    }

    public void setSystemHostName(String systemHostName) {
        this.systemHostName = systemHostName;
    }

    public String getSystemModel() {
        return systemModel;
    }

    public void setSystemModel(String systemModel) {
        this.systemModel = systemModel;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemType() {
        return systemType;
    }

    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }

    public String getSystemSerialNumber() {
        return systemSerialNumber;
    }

    public void setSystemSerialNumber(String systemSerialNumber) {
        this.systemSerialNumber = systemSerialNumber;
    }

    public String getSystemSku() {
        return systemSku;
    }

    public void setSystemSku(String systemSku) {
        this.systemSku = systemSku;
    }

    public String getSystemPower() {
        return systemPower;
    }

    public void setSystemPower(String systemPower) {
        this.systemPower = systemPower;
    }

    public String getSystemPowerState() {
        return systemPowerState;
    }

    public void setSystemPowerState(String systemPowerState) {
        this.systemPowerState = systemPowerState;
    }

    public JSONArray getSystemHostMacaddress() {
        return systemHostMacaddress;
    }

    public void setSystemHostMacaddress(JSONArray systemHostMacaddress) {
        this.systemHostMacaddress = systemHostMacaddress;
    }

    public SystemBoot getSystemBoot() {
        return systemBoot;
    }

    public void setSystemBoot(SystemBoot systemBoot) {
        this.systemBoot = systemBoot;
    }

    public SystemProcessorSummary getSystemProcessorSummary() {
        return systemProcessorSummary;
    }

    public void setSystemProcessorSummary(SystemProcessorSummary systemProcessorSummary) {
        this.systemProcessorSummary = systemProcessorSummary;
    }

    public SystemMemorySummary getSystemMemorySummary() {
        return systemMemorySummary;
    }

    public void setSystemMemorySummary(SystemMemorySummary systemMemorySummary) {
        this.systemMemorySummary = systemMemorySummary;
    }        

    public List<ProcessorSystem> getListProcessors() {
        return listProcessors;
    }

    public void setListProcessors(List<ProcessorSystem> listProcessors) {
        this.listProcessors = listProcessors;
    }

    public List<MemorySystem> getListMemorys() {
        return listMemorys;
    }

    public void setListMemorys(List<MemorySystem> listMemorys) {
        this.listMemorys = listMemorys;
    }

    public PowerControlSystem getPowerControlSystem() {
        return powerControlSystem;
    }

    public void setPowerControlSystem(PowerControlSystem powerControlSystem) {
        this.powerControlSystem = powerControlSystem;
    }

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }    

    public String getSystemManufacturer() {
        return systemManufacturer;
    }

    public void setSystemManufacturer(String systemManufacturer) {
        this.systemManufacturer = systemManufacturer;
    }    
    
}
