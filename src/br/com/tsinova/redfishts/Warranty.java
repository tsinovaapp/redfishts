package br.com.tsinova.redfishts;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;

public class Warranty {

    public static final String TYPE = "Warranty";
    public static final String PREFIX = "";

    private String systemUuid;
    private String serverProductSerial;

    private String typeService;
    private Date startDate;
    private Date endDate;
    private String status;

    private String getDateFormatted(Date date) {
        DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        return formatDate.format(date);
    }

    public JSONObject toJSONObject() throws Exception {

        JSONObject json = new JSONObject();
        
        //json.put(Util.NAME_DOC_ID, generateIdDoc());
        String id = "server_" + serverProductSerial
                + "_warranty_" + typeService;
        json.put(Util.NAME_DOC_ID, id);
        json.put(SystemRedfish.PREFIX + "uuid", systemUuid);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "warranty_type", typeService);
        json.put(PREFIX + "warranty_start_date", getDateFormatted(startDate) + "T00:00:00.000Z");
        json.put(PREFIX + "warranty_end_date", getDateFormatted(endDate) + "T23:59:59.999Z");
        json.put(PREFIX + "warranty_status", status);
        json.put("type", TYPE);
        return json;

    }

//    private String generateIdDoc() throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception{
//        
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        BigInteger hash = new BigInteger(1, md.digest(typeService.getBytes("UTF-8")));        
//        return systemUuid + "-" + TYPE + "-" + hash.toString(16);
//        
//    }
    public Warranty(String typeService, Date startDate, Date endDate, String status,
            String systemUuid, String serverProductSerial) {
        this.typeService = typeService;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;

        this.systemUuid = systemUuid;
        this.serverProductSerial = serverProductSerial;
    }

    public Warranty() {
    }

    public String getTypeService() {
        return typeService;
    }

    public void setTypeService(String typeService) {
        this.typeService = typeService;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSystemUuid() {
        return systemUuid;
    }

    public void setSystemUuid(String systemUuid) {
        this.systemUuid = systemUuid;
    }

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }

    @Override
    public String toString() {
        return "Warranty{" + "typeService=" + typeService + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status + '}';
    }

    public static List<Warranty> getListWarranty(String sn, String systemUuid, String serverProductSerial) throws Exception {

        DateFormat format = new SimpleDateFormat("MMM dd, yyyy");

        URL oracle = new URL("https://support.hpe.com/hpsc/wc/public/find?submitButton=Senden&rows[0].item.countryCode=BR&rows[0].item.serialNumber=" + sn);
        URLConnection yc = oracle.openConnection();

        String text = "";

        try (BufferedReader in = new BufferedReader(new InputStreamReader(
                yc.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                text += inputLine.trim();
            }
        }

        String regex = ".*<td id=\"generate_table_" + sn + "\".*<table(.*>)</table>.*";

        String content = text.replaceAll(regex, "$1");

        regex = ".*<tbody>(.*)</tbody>.*";

        content = content.replaceAll(regex, "$1");

        String lines[] = content.split("</tr>");

        regex = ".*<td.*>(.*)";

        List<Warranty> list = new ArrayList<>();

        for (String line : lines) {

            String columns[] = line.split("</td>");

            String status = columns[columns.length - 1].trim().replaceAll(regex, "$1");
            Date endDate = format.parse(columns[columns.length - 2].trim().replaceAll(regex, "$1"));
            Date startDate = format.parse(columns[columns.length - 3].trim().replaceAll(regex, "$1"));
            String typeService = columns[columns.length - 4].trim().replaceAll(regex, "$1");

            Warranty warranty = new Warranty(typeService, startDate, endDate, status, systemUuid, serverProductSerial);
            list.add(warranty);

        }

        return list;

    }

}
