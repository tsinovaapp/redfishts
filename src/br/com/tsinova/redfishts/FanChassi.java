package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class FanChassi {

    public static final String TYPE = "Fans";
    public static final String PREFIX = "fan_";

    private String id;
    private String name;
    private String location;
    private String health;
    private String state;
    private Integer reading;
    private String readingUnits;
    private String serverProductSerial;

    private FanChassi(Server server, String codChassi, JSONObject json, String serverProductSerial) throws Exception {
        this.serverProductSerial = serverProductSerial;
        name = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "FanName", ""), "");
        location = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Oem.Hp.Location", ""), "");
        health = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.Health", ""), "");
        state = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Status.State", ""), "");
        reading = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, json, "CurrentReading", null), null);
        readingUnits = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Units", ""), "");
        id = "server_" + serverProductSerial + "_chasssi_" + codChassi + "_fan_" + name;
    }

    public JSONObject toJSONObject() throws Exception {

        JSONObject json = new JSONObject();
        //json.put(Util.NAME_DOC_ID, generateIdDoc());
        json.put(Util.NAME_DOC_ID, id);
        json.put(PREFIX + "name", name);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "location", location);
        json.put(PREFIX + "health", health);
        json.put(PREFIX + "state", state);
        json.put(PREFIX + "reading", reading);
        json.put(PREFIX + "reading_units", readingUnits);
        json.put("type", TYPE);
        return json;

    }

//    private String generateIdDoc() throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception {
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        BigInteger hash = new BigInteger(1, md.digest(id.getBytes("UTF-8")));
//        return hash.toString(16);
//    }

    public static List<FanChassi> getListFans(Server server, String urlChassi, String codChassi, 
            String serverProductSerial) throws Exception {

        HttpResponse<JsonNode> request = Unirest.get(urlChassi + "Thermal/").
                basicAuth(server.getUser(), server.getPassword()).
                asJson();

        if (request.getBody() == null) {
            throw new Exception("No response to " + urlChassi + "Thermal/");
        }

        JSONObject json = request.getBody().getObject();

        JSONArray fans = json.getJSONArray("Fans");

        List<FanChassi> listFans = new ArrayList<>();

        for (int i = 0; i < fans.length(); i++) {
            listFans.add(new FanChassi(server, codChassi, fans.getJSONObject(i), serverProductSerial));
        }

        return listFans;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getReading() {
        return reading;
    }

    public void setReading(Integer reading) {
        this.reading = reading;
    }

    public String getReadingUnits() {
        return readingUnits;
    }

    public void setReadingUnits(String readingUnits) {
        this.readingUnits = readingUnits;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }
    
}
