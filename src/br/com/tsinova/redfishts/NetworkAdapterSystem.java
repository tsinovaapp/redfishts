package br.com.tsinova.redfishts;

import io.github.openunirest.http.HttpResponse;
import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NetworkAdapterSystem {

    public static final String TYPE = "NetworkAdapterSystem";
    public static final String PREFIX = "sys_";

    private String systemUuid;
    private String serverProductSerial;
    
    private String adapterName; // [Name]
    private String serialNumber; // [SerialNumber]
    private String firmware; // [Firmware.Current.VersionString]    
    private String macAddress; // [MacAddress]    
    private String ipv4; // [IPv4Addresses.Address]    
    private String ipv6; // [IPv6Addresses.Address]    
    private String name; // [Name]    
    private Integer speedMbps; // [SpeedMbps]
    private String statusState; // [Status.State]
    private String statusHealth; // [Status.Health]    

    private NetworkAdapterSystem(JSONObject json, JSONObject jsonPhysicalPorts, String systemUuid, String serverProductSerial) throws Exception {
        
        adapterName = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Name", ""), "");
        firmware = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "Firmware.Current.VersionString", ""), "");
        serialNumber = Util.convertObjectToString(Util.getValueJson(Type.TEXT, json, "SerialNumber", ""), "");
        speedMbps = Util.convertObjectToInteger(Util.getValueJson(Type.INTEGER, jsonPhysicalPorts, "SpeedMbps", null), null);
        name = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonPhysicalPorts, "Name", ""), "");
        macAddress = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonPhysicalPorts, "MacAddress", ""), "");
        ipv4 = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonPhysicalPorts, "IPv4Addresses.Address", ""), "");
        ipv6 = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonPhysicalPorts, "IPv6Addresses.Address", ""), "");
        statusState = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonPhysicalPorts, "Status.State", ""), "");
        statusHealth = Util.convertObjectToString(Util.getValueJson(Type.TEXT, jsonPhysicalPorts, "Status.Health", ""), "");

        this.systemUuid = systemUuid;
        this.serverProductSerial = serverProductSerial;

    }

    public JSONObject toJSONObject() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(SystemRedfish.PREFIX + "uuid", systemUuid);
        json.put("server_product_serial", serverProductSerial);
        json.put(PREFIX + "networkadapter_name", adapterName);
        json.put(PREFIX + "networkadapter_firmware", firmware);
        json.put(PREFIX + "networkadapter_serial", serialNumber);
        json.put(PREFIX + "networkadapter_port_speed_mbps", speedMbps == null ? 0 : speedMbps);
        json.put(PREFIX + "networkadapter_port_name", name);
        json.put(PREFIX + "networkadapter_port_mac_address", macAddress);
        json.put(PREFIX + "networkadapter_port_ipv4", ipv4);
        json.put(PREFIX + "networkadapter_port_ipv6", ipv6);
        json.put(PREFIX + "networkadapter_port_status_state", statusState);
        json.put(PREFIX + "networkadapter_port_status_health", statusHealth);        
        json.put("type", TYPE);
        return json;

    }

//    public String generateIdDoc() throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception {
//
//        MessageDigest md = MessageDigest.getInstance("MD5");
//        BigInteger hash = new BigInteger(1, md.digest((TYPE+"-"+macAddress).getBytes("UTF-8")));
//        return systemUuid + "-" + hash.toString(16);
//
//    }

    public static List<NetworkAdapterSystem> getListNetworkAdapterSystem(Server server, String urlSystem,
            String systemUuid, String serverProductSerial) throws Exception {

        ArrayList<String> urlsNetworkAdapters = getUrlsNetworkAdapters(server, urlSystem);

        List<NetworkAdapterSystem> listNetworkAdapters = new ArrayList<>();

        for (String urlNetworkAdapter : urlsNetworkAdapters) {

            HttpResponse<JsonNode> request = Unirest.get(urlNetworkAdapter).
                    basicAuth(server.getUser(), server.getPassword()).
                    asJson();

            if (request.getBody() == null) {
                throw new Exception("No response to " + urlNetworkAdapter);
            }

            JSONObject json = request.getBody().getObject();
            JSONArray ports = json.getJSONArray("PhysicalPorts");

            for (int i = 0; i < ports.length(); i++) {
                listNetworkAdapters.add(new NetworkAdapterSystem(json, ports.getJSONObject(i), systemUuid, serverProductSerial));
            }

        }

        return listNetworkAdapters;

    }

    private static ArrayList<String> getUrlsNetworkAdapters(Server server, String urlSystem) throws Exception {

        HttpResponse<JsonNode> request = Unirest.get(urlSystem + "NetworkAdapters/")
                .basicAuth(server.getUser(), server.getPassword()).asJson();

        JSONArray members = request.getBody().getObject().getJSONArray("Members");

        ArrayList<String> urlsNetworkAdapters = new ArrayList<>();

        for (int i = 0; i < members.length(); i++) {

            JSONObject member = members.getJSONObject(i);
            String path = member.getString("@odata.id");
            urlsNetworkAdapters.add(Util.getUrl(server.getUrl(), path));

        }

        return urlsNetworkAdapters;

    }

    public String getSystemUuid() {
        return systemUuid;
    }

    public void setSystemUuid(String systemUuid) {
        this.systemUuid = systemUuid;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIpv4() {
        return ipv4;
    }

    public void setIpv4(String ipv4) {
        this.ipv4 = ipv4;
    }

    public String getIpv6() {
        return ipv6;
    }

    public void setIpv6(String ipv6) {
        this.ipv6 = ipv6;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSpeedMbps() {
        return speedMbps;
    }

    public void setSpeedMbps(Integer speedMbps) {
        this.speedMbps = speedMbps;
    }

    public String getStatusState() {
        return statusState;
    }

    public void setStatusState(String statusState) {
        this.statusState = statusState;
    }

    public String getStatusHealth() {
        return statusHealth;
    }

    public void setStatusHealth(String statusHealth) {
        this.statusHealth = statusHealth;
    }

    public String getAdapterName() {
        return adapterName;
    }

    public void setAdapterName(String adapterName) {
        this.adapterName = adapterName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }    

    public String getServerProductSerial() {
        return serverProductSerial;
    }

    public void setServerProductSerial(String serverProductSerial) {
        this.serverProductSerial = serverProductSerial;
    }    
    
}
