package br.com.tsinova.redfishts;

public enum Type {
    
    INTEGER(1), TEXT(2), DOUBLE(3), LONG(4), ARRAY_JSON(5);
    
    public int value;
    
    Type(int value){
        this.value = value;
    }
    
}
